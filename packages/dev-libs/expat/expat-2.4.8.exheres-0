# Copyright 2007 Bryan Østergaard <kloeri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=libexpat project=lib${PN} release=R_${PV//./_} suffix=tar.xz ]

SUMMARY="Stream-oriented XML parser written in C"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    examples
    ( parts: binaries development documentation libraries )
"

DEPENDENCIES=""

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
    --with-xmlwf
    # Don't rebuild xmlwf man page, would need docbook-utils
    --without-docbook
    # Would use libbsd for arc4random_buf, but we can just use getrandom from
    # Linux 3.17+ with glibc[>=2.25] and musl[>=1.1.20]
    --without-libbsd
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( examples )
DEFAULT_SRC_CONFIGURE_TESTS=( '--with-tests --without-tests' )

src_install() {
    default

    expart binaries /usr/$(exhost --target)/bin
    expart development /usr/$(exhost --target)/include
    expart documentation /usr/share/{doc,man}
    expart libraries /usr/$(exhost --target)/lib
}

