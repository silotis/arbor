# Copyright 2008 Santiago M. Mola
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'libxslt-1.1.23.ebuild', which is:
#   Copyright 1999-2008 Gentoo Foundation

require gitlab [ prefix='https://gitlab.gnome.org' user='GNOME' new_download_scheme=true tag=v${PV} suffix=tar.bz2 ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ]
require python [ with_opt=true blacklist=none multibuild=false ]

SUMMARY="XSLT libraries and tools"
HOMEPAGE+=" http://www.xmlsoft.org/XSLT"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="crypt debug examples python"

DEPENDENCIES="
    build+run:
        dev-libs/libxml2:2.0[>=2.6.27]
        crypt? ( dev-libs/libgcrypt[>=1.1.92] )
        python? ( dev-libs/libxml2:2.0[>=2.6.27][python][python_abis:*(-)?] )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-1.1.27-m4.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-rebuild-docs
    # Fix html doc directory to include exheres revision
    --with-html-subdir=${PNVR}/html

    --disable-static
    # Always pass --with-debugger. It is required by third parties (see
    # e.g. Gentoo bug #98345)
    --with-debugger
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    "crypt crypto"
    "debug"
    "debug mem-debug"
    "python python ${PYTHON}"
)

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( FEATURES )

src_configure() {
    # Use the target's libgcrypt-config. This is safe because libgcrypt-config is a standalone shell
    # script with /bin/sh as shebang.
    LIBGCRYPT_CONFIG=/usr/$(exhost --target)/bin/libgcrypt-config \
        default
}

src_install() {
    default

    keepdir /usr/$(exhost --target)/lib/${PN}-plugins

    if ! option examples; then
        edo rm -rf "${IMAGE}"/usr/share/doc/${PN}-python-${PV}/examples
        # Empty DOCS variable in python/Makefile.am
        if option python; then
            # Remove empty dir
            edo rmdir "${IMAGE}"/usr/share/doc/${PN}-python-${PV}
        fi
    fi
}

