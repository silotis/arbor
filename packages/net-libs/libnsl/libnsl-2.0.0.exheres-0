# Copyright 2018-2021 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=thkukuk release=v${PV} suffix=tar.xz ]

SUMMARY="Public client interface library for NIS(YP) and NIS+"
DESCRIPTION="
This library contains the public client interface for NIS(YP) and NIS+. This code was formerly part
of glibc, but is now standalone to be able to link against TI-RPC for IPv6 support. The NIS(YP)
functions are still maintained, the NIS+ part is deprecated and should not be used anymore.
"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.2]
        virtual/pkg-config[>=0.9.0]
    build+run:
        net-libs/libtirpc[>=1.0.1]
        !sys-libs/glibc[<2.31-r3] [[
            description = [ Part of glibc when built with the deprecated nsl functionality ]
            resolution = upgrade-blocked-before
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --disable-static
)

