# Copyright 2009 Timothy Redaelli <timothy@redaelli.eu>
# Copyright 2013-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'mdadm-3.0.ebuild', which is:
#     Copyright 1999-2009 Gentoo Foundation

require systemd-service udev-rules

SUMMARY="The mdadm program controls Linux md devices (software RAID arrays)"
DESCRIPTION="
The mdadm program is used to create, manage, and monitor Linux MD (software
RAID) devices.  As such, it provides similar functionality to the raidtools
package.  However, mdadm is a single program, and it can perform
almost all functions without a configuration file, though a configuration
file can be used to help with some common tasks.
"
HOMEPAGE="https://www.kernel.org/pub/linux/utils/raid/${PN}"
DOWNLOADS="
    mirror://kernel/linux/utils/raid/${PN}/${PNV}.tar.xz
    mirror://debian/pool/main/m/${PN}/${PN}_4.2-3.debian.tar.xz
"

LICENCES="
    Artistic-2.0 [[ note = [ checkarray ] ]]
    GPL-2
"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

# Tests shouldn't be run on systems with active mdraid devices, since
# they mess with values in /proc and run on the system mdraid devices.
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
    run:
        sys-apps/util-linux[>=2.16]
    suggestion:
        virtual/mta [[ description = [ Send reports via email ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-4.2-mdmonitor-service.patch
)

DEFAULT_SRC_COMPILE_PARAMS=(
    CC="${CC}"
    CWFLAGS="-Wall"
    CXFLAGS="${CFLAGS}"
    BINDIR=/usr/$(exhost --target)/bin
    MAP_DIR=/run/mdadm
    all
)
DEFAULT_SRC_INSTALL_PARAMS=(
    CROSS_COMPILE="$(exhost --tool-prefix)"
    BINDIR=/usr/$(exhost --target)/bin
    SYSTEMD_DIR=${SYSTEMDSYSTEMUNITDIR}
    install-systemd
)

src_install() {
    default

    insinto /etc
    newins mdadm.conf-example mdadm.conf

    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins ${PN}.conf <<EOF
d /run/mdadm 0710 root root -
EOF

    # Debian checkarray script
    dobin ${WORKBASE}/debian/checkarray
    dodoc ${WORKBASE}/debian/README.checkarray
}

