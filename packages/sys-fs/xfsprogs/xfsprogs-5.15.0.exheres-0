# Copyright 2009 Daniel Mierswa <impulze@impulze.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service

SUMMARY="XFS userspace tools"
DESCRIPTION="Userspace utilities for the XFS filesystem including fsck and mkfs."
HOMEPAGE="https://xfs.org"
DOWNLOADS="https://www.kernel.org/pub/linux/utils/fs/xfs/${PN}/${PNV}.tar.xz"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( linguas: de pl )
"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/icu:=
        dev-libs/inih
        dev-libs/libedit
        dev-libs/userspace-rcu
        sys-apps/util-linux [[ note = [ for libblkid and libuuid ] ]]
        sys-fs/lvm2
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    OPTIMIZER=' '
    DEBUG=' '
    --enable-blkid
    --enable-editline
    --enable-gettext
    --enable-libicu
    --enable-librt
    --enable-lto
    --enable-scrub
    --disable-addrsan
    --disable-lib64
    --disable-termcap
    --disable-threadsan
    --disable-ubsan
    --with-systemd-unit-dir=${SYSTEMDSYSTEMUNITDIR}
)
DEFAULT_SRC_COMPILE_PARAMS=( V=1 )
DEFAULT_SRC_INSTALL_PARAMS=( DIST_ROOT="${IMAGE}" install-dev )

src_prepare() {
    edo sed \
        -e "/^PKG_DOC_DIR\t=/s:@pkg_name@:${PNVR}:" \
        -i include/builddefs.in

    default
}

src_install() {
    default

    edo rm "${IMAGE}"/usr/share/doc/${PNVR}/release.sh
}

