# Copyright 2008, 2009 Ali Polatel
# Copyright 2013 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'docutils-0.4-r3.ebuild' from Gentoo, which is:
#   Copyright 1999-2008 Gentoo Foundation

require pypi setup-py [ import=setuptools has_bin=true ]

SUMMARY="Set of tools for processing plaintext docs into HTML, XML, etc..."
DESCRIPTION="
Docutils is an open-source text processing system for processing plaintext documentation into
useful formats, such as HTML or LaTeX. It includes reStructuredText, the easy to read, easy to use,
what-you-see-is-what-you-get plaintext markup language.
"
HOMEPAGE+=" http://docutils.sourceforge.net"

REMOTE_IDS+=" sourceforge:docutils"

UPSTREAM_CHANGELOG="http://docutils.sourceforge.net/HISTORY.html"
UPSTREAM_RELEASE_NOTES="http://docutils.sourceforge.net/RELEASE-NOTES.html"

LICENCES="public-domain PYTHON BSD-2 GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    suggestion:
        dev-python/Pillow [[ description = [ Support for images ] ]]
        dev-python/Pygments [[ description = [ Support syntax highlighting within code directive ] ]]
"

install_one_multibuild() {
    # Tools + Docs
    dobin tools/*.py
    dodoc -r docs

    setup-py_install_one_multibuild
    SHEBANG=python${MULTIBUILD_TARGET}
}

src_install() {
    setup-py_src_install

    edo sed \
        -e "1 s/python$/${SHEBANG}/" \
        -i "${IMAGE}"/usr/$(exhost --target)/bin/*

    for bin in "${IMAGE}"/usr/$(exhost --target)/bin/*; do
        name="$(basename "${bin}")"
        dosym "${name}" /usr/$(exhost --target)/bin/"${name%.py}"
    done
}

test_one_multibuild() {
    PYTHONPATH="$(ls -d build/lib*)" edo ${PYTHON} test/alltests.py
}

