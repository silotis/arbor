# Copyright 2008-2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'gnupg-2.0.7.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require systemd-service [ systemd_files=[ ] systemd_user_files=[ doc/examples/systemd-user/{{dirmngr,gpg-agent}.{service,socket},gpg-agent-{browser,extra,ssh}.socket} ] ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require udev-rules [ udev_files=[ 80-${PN}-ccid.rules ] ]

SUMMARY="The GNU Privacy Guard, a GPL pgp replacement"
HOMEPAGE="https://www.gnupg.org"
DOWNLOADS="mirror://gnupg/gnupg/${PNV}.tar.bz2"

LICENCES="
    BSD-2 [[ note = [ regexp ] ]]
    BSD-3 [[ note = [ TinySCHEME (tests/gpgscm) ] ]]
    GPL-3
    || ( LGPL-2.1 LGPL-3 )
    MIT   [[ note = [ DNS resolver (dirmngr/dns.c) ] ]]
    Unicode-Data [[ note = [ regexp/UnicodeData.txt ] ]]
"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    doc
    hkps [[ description = [ Enable support for HKPS protocol using GnuTLS ] ]]
    keyboxd [[ description = [ Support for a experimental key database daemon ] ]]
    smartcard [[ description = [ Install the smartcard daemon ( includes CCID support ) ] ]]
    tofu [[ description = [ Support for the Trust on First Use trust model  ] ]]
    tpm2 [[ description = [ Allows binding keys to the local machine using TPM2 hardware ] ]]
    ( linguas: ca cs da de el eo es et fi fr gl hu id it ja nb pl pt ro ru sk sv tr uk zh_CN
               zh_TW )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.20.2]
        doc? ( sys-apps/texinfo )
    build+run:
        app-arch/bzip2
        dev-libs/libassuan[>=2.5.0]
        dev-libs/libgcrypt[>=1.9.1]
        dev-libs/libgpg-error[>=1.41]
        dev-libs/libksba[>=1.3.4]
        dev-libs/npth[>=1.2]
        net-misc/curl[>=7.10]
        sys-libs/readline:=
        sys-libs/zlib
        hkps? ( dev-libs/gnutls[>=3.0] )
        keyboxd? ( dev-db/sqlite:3[>=3.27] )
        smartcard? (
            dev-libs/libusb:1
            group/scard
        )
        tofu? ( dev-db/sqlite:3[>=3.27] )
        tpm2? ( app-crypt/tpm2-tss[>=2.4.0] )
    recommendation:
        app-crypt/pinentry [[ description = [
            Provides GTK+, Qt, and ncurses password prompts
        ] ]]
        smartcard? ( sys-apps/pcsc-lite ) [[ description = [
            Provides the default pcsc library for accessing smartcards
        ] ]]
"

AT_M4DIR=( m4 )

src_unpack() {
    default
    option smartcard && edo cp {"${FILES}","${WORK}"}/80-${PN}-ccid.rules
}

src_prepare() {
    autotools_src_prepare
    option smartcard && edo sed -e "s:@UDEVDIR@:${UDEVDIR}:" -i 80-${PN}-ccid.rules
}

src_configure() {
    local myconf=(
        CC_FOR_BUILD=$(exhost --build)-cc
        --enable-bzip2
        --enable-gpgsm
        --enable-libdns
        --enable-nls
        --enable-run-gnupg-user-socket
        --enable-zip
        --disable-build-timestamp
        --disable-ldap
        --disable-ntbtls
        --disable-selinux-support
        --disable-werror
        --without-capabilities # This option doesn\'t do anything.
        $(for lib in gpg-error ksba libgcrypt npth libassuan libiconv libintl;do
            echo "--with-${lib}-prefix=/usr/$(exhost --target)"
        done)
        $(option_enable hkps gnutls)
        $(option_enable keyboxd)
        $(option_enable smartcard scdaemon)
        $(option_enable smartcard ccid-driver)
        $(option_enable tofu)
        $(option_enable tpm2 tpm2d)
        $(expecting_tests --enable-tests --disable-tests)
    )

    if option keyboxd || option tpm2 ; then
        myconf+=( --enable-sqlite )
    else
        myconf+=( --disable-sqlite )
    fi

    econf "${myconf[@]}"
}

src_compile() {
    default
    option doc && emake html
}

src_test() {
    # Allow the tests to run its own instance of gpg-agent
    esandbox allow_net "unix:${WORK}/tests/openpgp/S.gpg-agent*"
    esandbox allow_net --connect "unix:${WORK}/tests/openpgp/S.gpg-agent*"
    esandbox allow_net "unix:/tmp/gpgscm-*/S.gpg-agent*"
    esandbox allow_net --connect "unix:/tmp/gpgscm-*/S.gpg-agent*"
    esandbox allow_net "unix:/tmp/gpgscm-*/S.dirmngr*"
    esandbox allow_net --connect "unix:/tmp/gpgscm-*/S.dirmngr*"

    default

    esandbox disallow_net --connect "unix:/tmp/gpgscm-*/S.dirmngr*"
    esandbox disallow_net "unix:/tmp/gpgscm-*/S.dirmngr*"
    esandbox disallow_net --connect "unix:/tmp/gpgscm-*/S.gpg-agent*"
    esandbox disallow_net "unix:/tmp/gpgscm-*/S.gpg-agent*"
    esandbox disallow_net --connect "unix:${WORK}/tests/openpgp/S.gpg-agent*"
    esandbox disallow_net "unix:${WORK}/tests/openpgp/S.gpg-agent*"
}

src_install() {
    default

    install_systemd_files

    if option doc; then
        insinto /usr/share/doc/${PNVR}/html
        doins doc/*.png doc/gnupg.html/*
    fi

    if option smartcard; then
        install_udev_files
        exeinto "${UDEVDIR}"
        doexe "${FILES}"/${PN}-ccid
    fi
}

