# Copyright 2008, 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'common-metadata.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require meson

export_exlib_phases src_prepare

SUMMARY="Typesafe callback system for standard C++"
DESCRIPTION='
libsigc++ implements a typesafe callback system for standard C++. It
allows you to define signals and to connect those signals to any
callback function, either global or a member function, regardless of
whether it is static or virtual.

libsigc++ is used by gtkmm to wrap the GTK+ signal system. It does not
depend on GTK or gtkmm.
'
HOMEPAGE="https://libsigcplusplus.github.io/libsigcplusplus/"
DOWNLOADS="mirror://gnome/sources/${PN}/$(ever range 1-2)/${PNV}.tar.xz"

LICENCES="LGPL-2.1"
SLOT="$(ever major)"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        doc? (
            app-doc/doxygen
            dev-lang/perl:*
            dev-libs/libxslt
            media-gfx/graphviz
        )
"

libsigc++_src_prepare() {
    meson_src_prepare

    # Fix docdir
    if ever at_least 3.0.7; then
        edo sed \
            -e "/install_docdir/s:/ book_name:/ '${PNVR}':" \
            -i docs/docs/reference/meson.build
        edo sed \
            -e "/install_tutorialdir/s:/ book_name:/ '${PNVR}':" \
            -i docs/docs/manual/meson.build
    else
        edo sed \
            -e "/install_docdir/s:/ book_name:/ '${PNVR}':" \
            -i docs/reference/meson.build
    fi
}

MESON_SRC_CONFIGURE_PARAMS=(
    # Would need boost::timer
    -Dbenchmark=false
    # Tests fail to build when enabled
    #-Dbuild-deprecated-api=false
    -Dbuild-examples=false
    -Dbuild-pdf=false
    # "Validate the tutorial XML file", would need xmllint -> libxml2 and
    # DocBook V5.0
    -Dvalidation=false
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    "doc build-documentation"
)

