# Copyright 2007 Bryan Østergaard <bryan.ostergaard@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ]
require option-renames [ renames=[ 'gnutls providers:gnutls' ] ]

SUMMARY="neon is an HTTP and WebDAV client library"
HOMEPAGE="https://notroj.github.io/${PN}/"
DOWNLOADS="${HOMEPAGE}${PNV}.tar.gz"

LICENCES="LGPL-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~x86"
MYOPTIONS="
    expat
    kerberos
    libproxy [[ description = [ Support for automatic configuration management through libproxy ] ]]

    ( providers: gnutls libressl openssl ) [[ number-selected = exactly-one ]]
    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.18]
    build+run:
        sys-libs/zlib
       !expat? ( dev-libs/libxml2:2.0 )
        expat? ( dev-libs/expat )
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
        libproxy? ( net-libs/libproxy:1 )
        providers:gnutls? ( dev-libs/gnutls )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl[>=0.9.7] )
    test:
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-0.29.4-sydbox.patch
    "${FILES}"/${PN}-0.30.1-addr_reverse-test-accept-fqdn.patch
    "${FILES}"/0001-pkg-config-is-a-tool.patch
)
AT_M4DIR=( macros )

src_prepare() {
    autotools_src_prepare

    # Disable a test that fails under sydbox
    edo sed -e '/T(session_cache)/d' -i test/ssl.c
}

src_configure() {
    local myconf=(
        --enable-shared
        $(option_with !expat libxml2)
        $(option_with expat)
        $(option_with kerberos gssapi)
        $(option_with libproxy)
        --with-ssl=$(option providers:gnutls && echo gnutls || echo openssl)
    )
    econf ${myconf[@]}
}

