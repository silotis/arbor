# Copyright 2009-2016 Wulf C. Krueger
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'pam-1.0.1.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require github [ user=linux-pam project=linux-pam release=v${PV} pnv=Linux-PAM-${PV} suffix=tar.xz ] \
    flag-o-matic \
    pam \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

export_exlib_phases pkg_pretend src_prepare src_configure src_test src_install

HOMEPAGE+=" http://www.linux-pam.org"
SUMMARY="Linux-PAM provides Pluggable Authentication Modules"

LICENCES="|| ( GPL-2 BSD-3 )"
SLOT="0"
MYOPTIONS="
    nis [[ description = [ Network Information Service support ] ]]
    parts: binaries configuration development documentation libraries
    ( providers: elogind systemd ) [[
        *description = [ Session tracking provider ]
        number-selected = at-most-one
    ]]
    ( providers: openssl libressl ) [[
        *description = [ To provide crypto algorithm for hmac ]
        number-selected = exactly-one
    ]]

    ( libc: musl )
    ( linguas: af am ar as az be bg bn bn_IN bs ca cs cy da de de_CH el eo es et eu fa fi fr ga gl
               gu he hi hr hu ia id is it ja ka kk km kn ko kw_GB ky lt lv mk ml mn mr ms my nb ne
               nl nn or pa pl pt pt_BR ro ru si sk sl sq sr sr@latin sv ta te tg th tr uk ur vi yo
               zh_CN zh_HK zh_TW zu )
"

DEPENDENCIES="
    build:
        sys-devel/flex
        sys-devel/gettext
    build+run:
        !libc:musl? ( dev-libs/libxcrypt:= )
        nis? ( net-libs/libtirpc )
    run:
        dev-libs/libpwquality
        sys-apps/shadow[>=4.9] [[ note = [ for yescrypt support ] ]]
        !libc:musl? ( dev-libs/libxcrypt:=[>=4.3] [[ note = [ for yescrypt support ] ]] )
        providers:elogind? ( sys-auth/elogind[pam] )
        providers:systemd? ( sys-apps/systemd )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
    suggestion:
        providers:elogind? ( sys-auth/pam_rundir ) [[
            description = [ Set XDG_RUNTIME_DIR automatically on login without systemd ]
        ]]
"

AT_M4DIR=( m4 )

# The configure.ac is using some magic to install to /lib if libdir
# is ${exec_prefix}/lib and prefix is /usr
# Make sure we *always* specify libdir to be on the safe side
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --includedir=/usr/$(exhost --target)/include/security
    --libdir=/usr/$(exhost --target)/lib
    --enable-db=no
    --enable-doc
    --enable-isadir=/usr/$(exhost --target)/lib/security
    --enable-nls
    --enable-openssl
    --enable-securedir=/usr/$(exhost --target)/lib/security
    --enable-unix
    --disable-audit
    --disable-econf
    --disable-passwdqc
    --disable-prelude
    --disable-regenerate-docu
    --disable-selinux
    --disable-usergroups
    --disable-vendordir
    --disable-Werror
    --with-kerneloverflowuid=65534
    --with-sysuidmin=101
    --with-uidmin=1000
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( nis )

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( doc/txt/* )

pam_build_pkg_pretend() {
    if [[ -z ${PAM_UPDATE} ]] && has_version "${CATEGORY}/${PN}" && \
        ! has_version "${CATEGORY}/${PN}[>=1.5.0]" ; then
        ewarn "As of pam-1.5.0, pam_tally and pam_tally2 have been removed in favour of pam_faillock,"
        ewarn "and pam_cracklib has been removed in favour of pam_pwquality."
        ewarn "Given the removal of these modules could result in you being locked out of your system,"
        ewarn "please ensure you either update your configs before pam (pam_faillock is already available"
        ewarn "and pam_pwquality will require libpwquality to be installed) or have a root session ready to apply"
        ewarn "changes with the help of eclectic config."
        ewarn "It is safe to just remove the pam_tally and pam_cracklib lines for the update."
        ewarn "It will reduce security until you've applied the new configuration but won't break your sessions."
        ewarn "Once you are ready, set PAM_UPDATE=Yes to continue the update"
        die "Ensure you have updated your PAM configs."
    fi

    if [[ ! -f /etc/pam.d/other ]]; then
        ewarn "We won't be able to run the tests. They require /etc/pam.d/other which you don't"
        ewarn "have. If you want to run the tests before installing ${PN} for the first time,"
        ewarn "copy ${FILES}/pam.d/other"
        ewarn "to /etc/pam.d/other and start the installation again. This is STRONGLY recommended."
    fi
}

pam_build_src_prepare() {
    edo mkdir -p doc/txt
    for readme in modules/pam_*/README; do
        edo cp -f "${readme}" doc/txt/README.$(dirname "${readme}" | sed -e 's|^modules/||')
    done

    # NOTE(somasis): fix libcrypt usage on musl
    [[ $(exhost --target) == *-musl* ]] && export ac_cv_search_crypt=no

    # NOTE(somasis): insecure, doesn't compile on musl either
    edo sed -e 's/pam_rhosts//g' -i modules/Makefile.am

    # We have to copy our pam.d files now so we can modify them
    edo cp -r "${FILES}"/pam.d "${WORK}"

    autotools_src_prepare
}

pam_build_src_configure() {
    CC_FOR_BUILD="$(exhost --build)-cc" \
    BUILD_CFLAGS="$(print-build-flags CFLAGS)" \
    BUILD_CPPFLAGS="$(print-build-flags CPPFLAGS)" \
    BUILD_LDFLAGS="$(print-build-flags LDFLAGS)" \
    default
}

pam_build_src_test() {
    if [[ ! -f /etc/pam.d/other ]]; then
        ewarn "Skipping tests. You don't have /etc/pam.d/other."
    else
        emake -j1 check
    fi
}

pam_build_src_install() {
    default

    keepdir /etc/security/limits.d
    keepdir /etc/security/namespace.d

    # NOTE(?) needs to be suid
    edo chmod u+s "${IMAGE}"/usr/$(exhost --target)/bin/unix_chkpwd

    # install base pam configuration files
    dopamd pam.d/*

    # remove environment file that is generated by eclectic env
    edo rm "${IMAGE}"/etc/environment

    expart binaries /usr/$(exhost --target)/bin
    expart configuration /etc
    expart development /usr/$(exhost --target)/include
    expart documentation /usr/share/{doc,man}
    expart libraries /usr/$(exhost --target)/lib
}

